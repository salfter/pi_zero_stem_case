Pi Zero Stem Case
=================

This is a remix of one of the designs at
https://www.thingiverse.com/thing:2516525, in which a notch is added over
the MicroSD-card slot to make card removal easier.
